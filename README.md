# HITB 2019 Amsterdam CTF Writeups

A collection of writups for select challenges from the 2019 Hack In The Box Amsterdam Capture The Flag competition.


## Introduction

The selection of challenges is arbitrary, these writeups are for the challenges which I made for the competition. Of course this means I know how they were intended to be solved and there may not be as much of a background in that part of the process leading up to the solution as might otherwise be the case. However I'll be including some notes about why certain design decision were made, what inspired the challenges, and some unintended solutions found by the teams playing the challenges during the competition.


## OTP 1, 2 & 3

A series of challenges to prediect OTP tokens.

[OTP](/OTP.md)


## Speedtest

Web challenge with object serialization in Ruby.

[Speedtest](/Speedtest.md)


## SPaaS

Secure memory disclosure challenge.

[SPaaS](/SPaaS.md)


## Agent Messaging

Simple crypto challenge based on AES-ECB.

[Agent Messaging](/AgentMessaging.md)


## Secure Memo Recorder

Crypto challenge to exploit a chosen plaintext attack against AES-ECB.

[Secure Memo Recorder](/SecureMemoRecorder.md)
