# OTP challenges

This is a series of three challenges where the objective is to be able to predict which OTP token the application will generate next.


## OTP 1

A copy of the challenge is still live at `insomnia247.nl:32173` so play along! This challenge is designed to work with a plain TCP socket connection so break out your netcat/telnet/whatever clients.

Fun fact; the port number 32173 is the base-36 integer representation of the string "OTP".

When connecting to the challenge you are first greeted with a "Proof of Work" challenge you need to compute the response to. This is not really part of the challenge itself but is more designed to give players the hint that you probably should not try to find a solution by doing an online brute force attak against the server. To solve the PoW challenge we need to brute force just two bytes (or four hex characters). Easy enough right? But first let's see what happens when we enter any random junk into the input;

```
$ nc insomnia247.nl 32173
Please provide proof of work for: SHA2(????365ce2f48e51b59ce19b47a16dd9) == bcf22da7e350448e90207926018540d0052cc83e35c1d64783407adadeef8d16
? lolhax
#!/usr/bin/env ruby
# frozen_string_literal: true

require 'digest'
require 'securerandom'
require 'socket'

# Prevent number from growing out of control over time
@max   = 100_000_000
# Use a secure random number so tokens cannot be predicted
@seed  = SecureRandom.random_number @max

@flag  = IO.read('flag.txt').chomp
@mutex = Mutex.new

def client_handler(client)
  # Call for PoW
  proof_of_work client

  # Show menu
  client_menu client

  client&.close
end

# Proof of Work function the client must complete
# to ensure our service is not abused DoS'ed.
def proof_of_work(client)
  value = SecureRandom.hex(16)
  work  = Digest::SHA2.hexdigest value
  client.puts "Please provide proof of work for: SHA2(????#{value[4..-1]}) == #{work}"
  client.print '? '
  pow = Digest::SHA2.hexdigest client.gets.chomp
  return true if pow == work

  # Show source as implementation reference if PoW fails
  client.puts File.read __FILE__
  client&.close
end

def client_menu(client)
  client.puts <<~ENDOFMENU
    1: Generate OTP token
    2: Validate OTP token
    3: Quit
  ENDOFMENU
  client.print '? '
  client_menu_handler client, client.gets.chomp.to_i
end

def client_menu_handler(client, input)
  case input
  when 1
    show_token client
  when 2
    validate_token client
  else
    client&.close
  end
end

# Generate token using cryptographically secure hash function
def next_token(token = '')
  @mutex.synchronize do
    token = Digest::SHA2.hexdigest @seed.to_s
    if @seed < @max
      @seed += 1
    else
      @seed = 0
    end
  end
  token[0..16]
end

def show_token(client)
  client.puts next_token
  client&.close
end

def validate_token(client)
  client.puts 'Please provide your OTP token'
  client.print '? '
  if client.gets.chomp == next_token
    client.puts @flag
  else
    client.puts 'Invalid token'
  end
  client&.close
end

# Start up the server
server = TCPServer.new 'OTP'.to_i 36
loop do
  Thread.fork(server.accept) do |client|
    client_handler client
  rescue StandardError => e
    # Just log errors to stdout
    puts "Error: #{e}"
  ensure
    client&.close
  end
end
^C
```

Well that's handy! It's the source code for the service.

For our solver scripts we'll be using python and [Pwntools/Pwnlib](https://github.com/Gallopsled/pwntools). You could certainly do without pwntools, but it offers us some quick and easy building blocks to build our solving scripts, so let's start by connecting to the service and solving the PoW.

### Solving the PoW

Since we now have the source code for the application we can double-check how the PoW is generated;
``` ruby
value = SecureRandom.hex(16)
work  = Digest::SHA2.hexdigest value
client.puts "Please provide proof of work for: SHA2(????#{value[4..-1]}) == #{work}"
client.print '? '
pow = Digest::SHA2.hexdigest client.gets.chomp
return true if pow == work
```

We can see that a random hex string is generated and hashed, then we're given all but the first four hex characters. Doesn't look like anything fishy is going on here, but it's good to notice the compare `if pow == work` is trying to match our input against the whole hex string so we need to submit the whole solution and not just the first four hex chars. Let's start writing our solver by just connecting to the service and solving the Proof of Work challenge.

``` python
from pwn import *
import re

# Connect to the service and read from it
remote = remote('insomnia247.nl', 32173)
line   = remote.recvline()

# Regexp out the Proof of Work we have to solve
match  = re.search(r"Please provide proof of work for: SHA2\(\?\?\?\?([0-9a-f]+)\) == ([0-9a-f]+)", line)
suffix = match.group(1)
proof  = match.group(2)

# Read until we get the prompt asking for our input
log.info('PoW challenge: %s\n', proof)
remote.recvuntil('? ')

# Use pwnlib's bruteforce and hash functions to find a match
key = pwnlib.util.iters.bruteforce(lambda x: pwnlib.util.hashes.sha256sumhex(x + suffix) == proof, '0123456789abcdef', length = 4)

# Submit our solution to the service and drop into an interactive console
remote.sendline(key + suffix)
remote.interactive()
```

As you can see, we're making liberal use of Pwnlib's sockets, brute force primitives and hashing functions. Let's run the script and see what happens;

```
$ python otp1.py
[+] Opening connection to insomnia247.nl on port 32173: Done
[*] PoW challenge: b49dd004a31f39ae80e8823fe09659fecca62b7ce96c5bb9ba7b98b30037e445
[+] Bruteforcing: Found key: "7742"
[*] Switching to interactive mode
1: Generate OTP token
2: Validate OTP token
3: Quit
? $ 
```

Hey! The PoW is solved and we now get to interact with a menu of some sort. If we take a peek back at the source code of the service we can find this menu and the functions that get called for each menu option.

When we pick option 1 it shows us a new token and when we pick option 2 we get prompted to enter a token.

Design note; When building the challenge I never thought about having to press enter after the menu selection but a few people did complain it bothered them during the competition. I concidered changing it (it's an easy fix) but I opted not to so we would not break scripts people had already written by that point in the competition.


### Finding the bug

We have the source for the service so let's take a look and see if we can spot the bug. At the top of the source there's just some library imports and a bit of setup. It looks like the flag is read from a file on disk and placed into a global variable; `@flag  = IO.read('flag.txt').chomp`. Since our eventual goal is to get a hold of that flag we'll take a look at where it's used, that might give us some clues about what we need to do.

There is a `validate_token` funcion where the `@flag` variable is used
``` ruby
  client.puts 'Please provide your OTP token'
  client.print '? '
  if client.gets.chomp == next_token
    client.puts @flag
```

It seems like if we manage to submit the same token as the `next_token` function generates, we'll get our flag. Cool beans! Let's see what the `next_token` function does. It doesn't look too complicated, but let's strip out some stuff we don't care about for now and focus on the core of the function;

``` ruby
token = Digest::SHA2.hexdigest @seed.to_s
if @seed < @max
  @seed += 1
else
  @seed = 0
end
token[0..16]
```

So the OTP token is just the first 16 chars of the sha256 hexdigest of the global variable `@seed`, and a little `if` to wrap the seed back to 0 in case it reaches some variable `@max`. Also of interest is that we can see it will just increment the `@seed` variable, so there's definitely a predictable pattern to these OTP tokens. If we go back to the top of our source we find that both `@seed` and `@max` are defined there;

``` ruby
# Prevent number from growing out of control over time
@max   = 100_000_000
# Use a secure random number so tokens cannot be predicted
@seed  = SecureRandom.random_number @max
```

It looks like we won't know what the seed will be set to, but we know it's some number between 0 and 100 million. Hey, we just saw that it also wraps back to 0 if the seed gets to `@max`, that means there are only 100 million possible tokens the service can generate, and since `@seed` is incremented we also know the order in which tokens will be generated.

We could certainly pre-compute 100 million sha256 hashes in a reasonable amount of time. We can use menu option 1 to get the current token, look up that token in our pre-computed list and see which token the service will generate next by just looking at the next entry in our list.

### Writing the exploit

There's a multitude of ways we could implement the precomputing and lookup of the tokens. We could stick them all into something like Redis, or even just a flat textfile. If you have enough compute power to throw at it, you can even do the brute force in real time. (I tried and it is fast enough. Besides, I paid for that AMD Threadripper and dangit I'm going to use it!)

Basically the only thing we need to add to our PoW solving script is to tell it to get a token;
``` python
r.sendline('1')
lasttoken = r.recvline().rstrip('\n')
log.info('Got OTP token: %s\n', lasttoken)
```
Then do our lookup or brute force;
``` python
seed  = pwnlib.util.iters.mbruteforce(lambda x: pwnlib.util.hashes.sha256sumhex(x)[0:17] == lasttoken, '0123456789', length = 8)
token = pwnlib.util.hashes.sha256sumhex(str(int(seed) + 1))[0:17]
```
Note that we are using the `mbruteforce` function this time, which is Pwnlib's multithreaded brute force function.

After this it's just a matter of connecting back to the service and submitting our solution to get the flag.

## OTP 2

The OTP 2 challenge is still live at `insomnia247.nl:32173`. When we connect to it we see the familiar sight of the PoW challenge. Entering a garbage answer we see that we can still get the source for the challenge, and you might notice that it looks pretty similar to the source of OTP 1. Let's look at a diff of the sources and see if anything interesting has changed.

``` diff
diff --git a/otp1.rb b/otp2.rb
index a386e4c..248e9c3 100644
--- a/otp1.rb
+++ b/otp2.rb
@@ -5,20 +5,22 @@ require 'digest'
 require 'securerandom'
 require 'socket'
 
-# Prevent number from growing out of control over time
-@max   = 100_000_000
+# Much larger number to prevent looping from happening
+@max   = 100_000_000_000_000_000
 # Use a secure random number so tokens cannot be predicted
 @seed  = SecureRandom.random_number @max
+# Use a secret key as part of the token to make prediction impossible
+@key   = IO.read('secret.txt').chomp
 
 @flag  = IO.read('flag.txt').chomp
 @mutex = Mutex.new
 
-def client_handler(client)
+def client_handler(client, seed)
   # Call for PoW
   proof_of_work client
 
   # Show menu
-  client_menu client
+  client_menu client, seed
 
   client&.close
 end
@@ -38,22 +40,22 @@ def proof_of_work(client)
   client&.close
 end
 
-def client_menu(client)
+def client_menu(client, seed)
   client.puts <<~ENDOFMENU
     1: Generate OTP token
     2: Validate OTP token
     3: Quit
   ENDOFMENU
   client.print '? '
-  client_menu_handler client, client.gets.chomp.to_i
+  client_menu_handler client, client.gets.chomp.to_i, seed
 end
 
-def client_menu_handler(client, input)
+def client_menu_handler(client, input, seed)
   case input
   when 1
-    show_token client
+    show_token client, seed
   when 2
-    validate_token client
+    validate_token client, seed
   else
     client&.close
   end
@@ -62,25 +64,20 @@ end
 # Generate token using cryptographically secure hash function
 def next_token(token = '')
   @mutex.synchronize do
-    token = Digest::SHA2.hexdigest @seed.to_s
-    if @seed < @max
-      @seed += 1
-    else
-      @seed = 0
-    end
+    token = Digest::SHA2.hexdigest token
   end
   token[0..16]
 end
 
-def show_token(client)
-  client.puts next_token
+def show_token(client, seed)
+  client.puts next_token(seed)
   client&.close
 end
 
-def validate_token(client)
+def validate_token(client, seed)
   client.puts 'Please provide your OTP token'
   client.print '? '
-  if client.gets.chomp == next_token
+  if client.gets.chomp == next_token(seed)
     client.puts @flag
   else
     client.puts 'Invalid token'
@@ -88,15 +85,30 @@ def validate_token(client)
   client&.close
 end
 
+# Pull manipulation of global state out to separate
+# function for scaling, performance and security reasons.
+def increment
+  @mutex.synchronize do
+    if @seed < @max
+      @seed += 1
+    else
+      @seed = 0
+    end
+  end
+end
+
 # Start up the server
-server = TCPServer.new 'OTP'.to_i 36
+server = TCPServer.new 'OTP'.to_i(36) + 1
 loop do
   Thread.fork(server.accept) do |client|
-    client_handler client
+    client_handler client, "#{@key}#{@seed}"
   rescue StandardError => e
     # Just log errors to stdout
     puts "Error: #{e}"
   ensure
+    # Always increment the seed so if anything
+    # fails we will not leave a predictable token.
+    increment
     client&.close
   end
 end
```

Oof. That's quite a list so let's break it down a little;

The `@max` value has been made much larger, so precomputing is probably not a viable strategy this time.


``` diff
-# Prevent number from growing out of control over time
-@max   = 100_000_000
+# Much larger number to prevent looping from happening
+@max   = 100_000_000_000_000_000
```
There also appears to be some additional secret loaded from a file. The comment mentions it is used as part of the token, so we likely won't be able to compute the tokens at all without that.

``` diff
+# Use a secret key as part of the token to make prediction impossible
+@key   = IO.read('secret.txt').chomp
```

It looks like these two things were done to address the vulnerability in OTP 1, so we'll keep looking at the diff to see what else has changed.

There's a few places where the seed is now passed as a function parameter, and near the bottom we see that indeed the secret key mentioned before is used as part of the seed now;
``` diff
-    client_handler client
+    client_handler client, "#{@key}#{@seed}"
```

There is one other interesting change. It seems the token seed increment/wrap code has been pulled out from the `next_token` function to a separate function called `increment` which is called from only one place; The `ensure` block in the main server socket accept loop. Let's have a closer look at that.

### Finding the bug

It looks like the significant change is what happened to the `next_token` and `increment` functions, and the observant amongst you may have already noticed that there is a pretty important change in the application logic which happened there. All the code is still inside of a mutex, but there are now two separate critical sections in distinct mutex lock locations rather than having the whole section inside one mutex.
``` diff
 def next_token(token = '')
   @mutex.synchronize do
-    token = Digest::SHA2.hexdigest @seed.to_s
-    if @seed < @max
-      @seed += 1
-    else
-      @seed = 0
-    end
+    token = Digest::SHA2.hexdigest token
   end
   token[0..16]
 end

+# Pull manipulation of global state out to separate
+# function for scaling, performance and security reasons.
+def increment
+  @mutex.synchronize do
+    if @seed < @max
+      @seed += 1
+    else
+      @seed = 0
+    end
+  end
+end
```
Looking at where `increment` function is called we can start to see the bug; The `increment` function is called at the very end when the client closes their connection.
``` diff
 loop do
   Thread.fork(server.accept) do |client|
-    client_handler client
+    client_handler client, "#{@key}#{@seed}"
   rescue StandardError => e
     # Just log errors to stdout
     puts "Error: #{e}"
   ensure
+    # Always increment the seed so if anything
+    # fails we will not leave a predictable token.
+    increment
     client&.close
   end
```
This means a client can connect, cause the `next_token` function to be invoked by having the service generate a new token for us, but leave the connection open. Now we connect with a second client and submit that exact same token we got from our first client. Since the `increment` function still has never been called, the service will generate the same token a second time when the `next_token` function is called to validate our token submitted by the second client.

### Writing the exploit

We have a classic connection state race condition to exploit here, but really there's barely any work to do. We can recycle the PoW code we used for OTP 1 but have it set up two concurrent connections this time. Then all we have to do is take a token from client 1 and stick it into client 2;
``` python
#!/usr/bin/env python

from pwn import *
import re

remote1 = remote('insomnia247.nl', 32174)
remote2 = remote('insomnia247.nl', 32174)

# Get connection 1 into ready state
line   = remote1.recvline()
m      = re.search(r"Please provide proof of work for: SHA2\(\?\?\?\?([0-9a-f]+)\) == ([0-9a-f]+)", line)
suffix = m.group(1)
proof  = m.group(2)

log.info('PoW challenge 1: %s\n', proof)
remote1.recvuntil('? ')

key = pwnlib.util.iters.bruteforce(lambda x: pwnlib.util.hashes.sha256sumhex(x + suffix) == proof, '0123456789abcdef', length = 4)
remote1.sendline(key + suffix)
remote1.recvuntil('? ')
log.success('Connection 1 ready')

# Get connection 2 into ready state
line  = remote2.recvline()
m      = re.search(r"Please provide proof of work for: SHA2\(\?\?\?\?([0-9a-f]+)\) == ([0-9a-f]+)", line)
suffix = m.group(1)
proof  = m.group(2)

log.info('PoW challenge 2: %s\n', proof)
remote2.recvuntil('? ')

key = pwnlib.util.iters.bruteforce(lambda x: pwnlib.util.hashes.sha256sumhex(x + suffix) == proof, '0123456789abcdef', length = 4)
remote2.sendline(key + suffix)
remote2.recvuntil('? ')
log.success('Connection 2 ready')

# We are now all feng-shui and ready to exploit the race condition
remote1.sendline('1')
remote2.sendline('2')

# Wait for token to be ready on remote 1
token = remote1.recvline().rstrip('\n')
log.info('Got OTP token: %s\n', token)

# Stick it remote 2
remote2.recvuntil('? ')
remote2.sendline(token)
log.success(remote2.readline())

# Close out connections
remote1.close()
remote2.close()
```

## OTP 3

A copy of the challenge is live at `insomnia247.nl:32175`.

Alright, just one more to go. We'll start the same way as for OPT 2, grab the source code and diff it with that of the previous challenge. Right away we'll spot some interesting changes at the top of our diff;

It seems they're using base64 encoding and openssl for something now
``` diff
+require 'base64'
 require 'digest'
+require 'openssl'
```
And the secret key is now randomly generated. But we can also learn how long the secret is; 3 bytes (hex encoded).
``` diff
-# Use a secret key as part of the token to make prediction impossible
-@key   = IO.read('secret.txt').chomp
+# Generate a new random key every time we restart the application
+@key   = SecureRandom.hex(3)
```

We can also see the changes made in OTP 2 have mostly been reverted. The `increment` function is still separate, but it is now called from within the same mutex block in the `next_token` function. Our race condition exploit from OTP 2 will not work anymore.

### Finding the bug

We'll take a look at the important bits of our diff where the new application logic is. There's two big chunks, the `next_token` and `validate_token` functions. We'll start with the `next_token` function.
``` ruby
# Generate token using cryptographically secure hash function
def next_token(seed = @seed.to_s, salt = SecureRandom.random_bytes(4), algo = 'SHA256', token = '')
  @mutex.synchronize do
    salt      = Base64.strict_encode64 salt
    timestamp = (Time.now.to_i / 100) * 100
    hmac      = OpenSSL::HMAC.hexdigest(algo, @key, "#{salt}:#{seed}:#{algo}:#{timestamp}")
    token     = "#{salt}:#{algo}:#{hmac}"
    increment
  end
  Base64.strict_encode64 token
end

```
The token has changed from a sha256 hexdigest to a more complex format. There is an HMAC calculated for a randomly generated salt, the seed, the HMAC algorithm being used, and a rounded off timestamp. The algorithm and salt are base64-encoded into our token along with the caluclated HMAC. In theory this means we should get a token that is a base64 string which decodes into these parts again.
```
$ python otp1.py 
[+] Opening connection to insomnia247.nl on port 32175: Done
[*] PoW challenge: 379faa05483696d5b8797cceccee3fb3cb9c54bd8286507786fe1c1800edb2fd
[+] Bruteforcing: Found key: "94ab"
[*] 1: Generate OTP token
    2: Validate OTP token
    3: Quit
    ? 
[*] Got OTP token: Z3oxUUl3PT06U0hBMjU2OjliYjY1ODFlYzA2Y2U0NDQ0YjRkOWQ2NmRiZDA3ZDRiNmEzNTcyODdlMGM1NDYxM2UzZGMzNmYyYTBmZjIxODE=
[*] Closed connection to insomnia247.nl port 32175

$ base64 -d <<<Z3oxUUl3PT06U0hBMjU2OjliYjY1ODFlYzA2Y2U0NDQ0YjRkOWQ2NmRiZDA3ZDRiNmEzNTcyODdlMGM1NDYxM2UzZGMzNmYyYTBmZjIxODE=
gz1QIw==:SHA256:9bb6581ec06ce4444b4d9d66dbd07d4b6a357287e0c54613e3dc36f2a0ff2181
```
It looks good, indeed we have a seed (base64 encoded a second time), the algorithm used, and the hex encoded HMAC signature data. There's nothing concrete yet that tells us what the bug is, but we can start to see a few potential targets. The seed and the algorithm are exposed to us an under our control if we submit a token to the service. To submit a valid token it looks like we'll have to find out two application secrets; the `@key` variable, and what the `@seed` value is. The other variables of the token are pretty straight forward; The timestamp we can just calculate ourselves, and the salt and the algorithm are given to us in the encoded token data.

We'll continue our exploration by looking at the `validate_token` function to see if we can spot any weakness in how it is checked.
``` ruby
def validate_token(client)
  client.puts 'Please provide your OTP token'
  client.print '? '

  input = client.gets.chomp
  begin
    input    = Base64.decode64(input)
    input    = input.split(':')
    input[1] = Base64.decode64(input[1])
  rescue StandardError => _e
    client.puts 'Failed to decode input'
    return
  end
```
The first few lines are just getting the token from the client. Then the token is decoded and split into the individual parts. Then the salt is decoded again. Any error in this decoding step will cause an error and close the connection. There doesn't appear to be any checks on how many `:` separated fields there are, and little checks so far on what they contain. But something odd is going on here. We saw the salt was base64 encoded before, and that it was the first element of the token. Should it not be base64 decoding `input[0]`?
``` ruby
  if input[0].to_i < @seed - 1
    client.puts 'Token replay attack detected!'
    client&.close
    return
  end
```
Here we also see `input[0]` being compared to the seed. It appears we are expected to prepend the seed to the token we submit. We can also see that if the seed is too small, the server will return an error. But hey! This error message gives us something important, we can distinguish between a seed that's too small, and a seed that's not too small. We should be able to abuse this to mount a binary search and figure out the value of `@seed` by observing the error returned by the server. That's one of the two secrets we need to generate a valid token!
``` ruby
  hmac = Base64.decode64(next_token((@seed - 1).to_s, input[1], input[2])).split(':').last

  if input[3] == hmac
    client.puts @flag
  else
    client.puts 'Invalid token'
  end
  client&.close
end
```
The rest of the function seems to be just to reconstruct the input for the HMAC function and check it against the HMAC data submitted in our token.

Side note: There is a second bug in this function which provides an unintended solution to this challenge. Can you spot it yet? We'll delve into that one a little later.

Alright, so we have one of our two application secrets, but if we think back to how the `@key` variable was generated we'll recall it was only 3 bytes long. Since we should have all the other parts of the puzzle in place now, brute forcing those last 3 bytes shouldn't be too difficult.

### Writing the exploit

We now know what we need to do to forge a valid token;
 - Binary search for `@seed` value by abusing error feedback
 - Get a valid token from the service to compare against
 - Brute force `@key` value by comparing against valid token
 - Generate a forged token and submit to the service

#### Binary search for seed

Since I'm lazy and we want a little robustness against tokens being generated while we're performing the attack we are just going to approximate it to about 5 under or over the actual value. We can just do a little extra brute force work later to figure out what the real valid seed is. (I **really** want to get some use out of that Threadripper.)

``` python
def test_seed(mini, maxi):
  g = int(((maxi - mini) / 2) + mini)
  r = remote('insomnia247.nl', 32175)
  do_pow(r)
  r.recvuntil('? ')
  r.sendline('2')
  r.recvuntil('? ')
  r.sendline(make_submit_token(g, make_token(g)))
  result = r.recvline()
  r.close()

  if result == "Token replay attack detected!\n":
    return [g, maxi]
  else:
    return [mini, g]

def main():
  mini = 0
  maxi = 100000000000000000

  while (maxi - mini) > 5:
    [mini, maxi] = test_seed(mini, maxi)
    log.info("%s <-> %s (%s)" % (mini, maxi, maxi - mini))
```
All we're doing here is making a guess that's about halfway between our minimum and maximum guess values and seeing if the value is too small or not. If it is, we adjust our lower bound, if it's not, we adjust our upper bound. When the two bounding values get close enough to eachother we'll call it good enough and move on. (Note that we've made the Proof of Work solver a function named `do_pow`. Just to make things a little easier on ourselves.)

#### Brute forcing the key

First we need to grab a fresh token from the server so we know our the seed will be in our range and that we have a predicatble timestamp. Then we loop over our possible values for the seed and see if we can brute force a valid 3-byte key for any of them.
``` python
valid = get_token()                                     # Grab a token
ts    = str((int(time.time())/100)*100)                 # Predict the timestamp
parts = pwnlib.util.fiddling.b64d(valid).split(':')     # Split out the token data

key   = ''
seed  = ''

# Allow 5 extra seeds in case the seed was incremented in the meantime
for i in range(mini, maxi + 5):
  seed = i
  key  = pwnlib.util.iters.mbruteforce(
    lambda x: make_token(i, salt=pwnlib.util.fiddling.b64d(parts[0]), timestamp=ts, key=x) == valid,
    '0123456789abcdef',
    length = 6
  )
  if key is not None:
    break
```

Once we find a working key, all we need to do is do a little string concat, HMAC, and base64 here and there to forge an accepted token and get the flag.

### Unintended solution(s)

I made a mistake when writing the `validate_token` function which provides a shortcut to solving this challenge.
``` ruby
  hmac = Base64.decode64(next_token((@seed - 1).to_s, input[1], input[2])).split(':').last
```
I should have used the user-provided `input[0]` to validate the submitted seed matches that in the HMAC data. Using `@seed` instead means the correct seed is always submitted to the function and the whole part of trying to figure out what the value of `@seed` is can be skipped by submitting a value close to the maximum value it can take. This way it will (almost) always pass the replay check but still have a valid seed during hmac validation.

But wait, there's more!

Since we validate the hmac data against `@seed - 1` we don't even need to figure out the key. As long as the timestamp generated by the `next_token` function has not changed yet, the only difference in the data being HMAC'ed is the seed value. Since you can view the previous HMAC data by requesting a token, it is possible to submit this data back to the server with any large enough seed value and get the flag back.

I was made aware of this after the CTF when reading a [writeup by team Beunhazen](https://beunhazen.net/2019/05/27/hitbams19-otp-writeup.html). Go read their writeups if you're at all interested, they're really good!
